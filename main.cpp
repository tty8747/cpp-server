#include <iostream>
#include <fstream>
#include <uwebsockets/App.h>
#include <nlohmann/json.hpp>
using namespace std;
using json = nlohmann::json;

struct UserData {
  int id;
  string name;
};

int main()
{
  // std::cout << "uwebsockets test is ok\n" << 123 << std::endl;
  ifstream html_file("index.html"); // open the file
  stringstream buffer;
  buffer << html_file.rdbuf(); // read file into buffer
  int latest_user_id = 10;
  uWS::App().get("/", [&buffer](auto *res, auto *req) {

      /* You can efficiently stream huge files too */
      res->writeHeader("Content-Type", "text/html; charset=utf-8")->end(buffer.str());

  }).ws<UserData>("/*", {
      .open = [&latest_user_id](auto *ws) {
      UserData* user_data = ws->getUserData();
      user_data->id = latest_user_id++;
      user_data->name = "unnamed_user #" + to_string(user_data->id);

      cout << "User connected: " << user_data->id << endl;

      ws->subscribe("public_channel");
      ws->subscribe("user" + to_string(user_data->id));
      },
      .message = [](auto *ws, std::string_view message, uWS::OpCode opCode) {
	UserData* user_data = ws->getUserData();
	json message_data = json::parse(message);
	if (message_data["command"] == "public_msg") {
	  json payload;
	  payload["command"] = "public_msg";;
	  payload["text"] = message_data["text"];
	  payload["user_from"] = user_data->id;
	  payload["user_name"] = user_data->name;
	  ws->publish("public_channel", payload.dump());
	  cout << "User sent public message: " << user_data->id << endl;
	}
	if (message_data["command"] == "private_msg") {
	  json payload;
	  payload["command"] = "private_msg";
	  payload["text"] = message_data["text"];
	  payload["user_from"] = user_data->id;
	  payload["user_name"] = user_data->name;
	  ws->publish("user" + to_string(message_data["user_to"]), payload.dump());
	  cout << "User sent private message: " << user_data->id << " to " << message_data["user_to"] << endl;
	}
	// { "command": "set_name", "name": "goto" }
	if (message_data["command"] == "set_name") {
	  if (message_data.contains("name")) {
	    string name = message_data["name"];
	    if (name.length() < 3 || name.length() > 255) {
	      json payload = { {"error", "Name length is incorrect"} };
	      ws->send(payload.dump(), uWS::OpCode::TEXT);
		cout << "Name length is incorrect: " << user_data->name << endl;
	    } else {
		user_data->name = message_data["name"];
		cout << "User set name to " << user_data->name << endl;
	    }
	  }
	}
	// { "command": "status", "online": "true/false", "name": "goto" } => public_channel
      }

  }).listen(9001, [](auto *listenSocket) {

      if (listenSocket) {
	  std::cout << "Listening on port " << 9001 << std::endl;
      }

  }).run();
}
