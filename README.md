# Server on c++ [(uWebSockets)](https://github.com/uNetworking/uWebSockets)

- Install dependencies
- `sudo vcpkg install uwebsockets nlohmann-json`
- `$ make`

> For help: https://json.nlohmann.me/integration/cmake/#embedded

Browser console(2+ tabs)  
- `ws = new WebSocket("ws://localhost:9001/"); ws.onmessage = ({data}) => console.log("From server", data)`

public message:
- `payload = {"command": "public_msg", "text":"Hey, people!"}`
- `ws.send(JSON.stringify(payload));`
  
private message:
- `payload = {"command": "private_msg", "text":"hey, user 10!", "user_to": 10}`
- `ws.send(JSON.stringify(payload));`
  
user name:
- `ws.send('{"command": "set_name", "name": "g"}')`
- `ws.send('{"command": "set_name", "name": "goto"}')`
