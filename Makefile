.DEFAULT_GOAL := all

# the same that .DEFAULT_GOAL for older version of make (<= 3.80)
# .PHONY: default
# default: clean;

.PHONY: all
all: build run clean

.PHONY: build
build:
	$(info -> Build:)
	mkdir -p ./build
	cmake -B ./build -S . "-DCMAKE_TOOLCHAIN_FILE=/opt/vcpkg/scripts/buildsystems/vcpkg.cmake"
	cmake --build ./build

.PHONY: run
run:
	$(info )
	$(info -> Run:)
	./build/bin/jarv_serv

.PHONY: clean
clean:
	$(info )
	$(info -> Clean:)
	rm -r ./build

.PHONY: messages
messages:
	$(info test info message)
	$(warning test warning message)
	$(error test error message)
